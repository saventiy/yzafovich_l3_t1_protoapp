package com.example.lesson2task2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ListOfStoriesFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private ArrayList<Data> arrData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_of_stories_fragment, container, false);

        mRecyclerView = v.findViewById(R.id.list_of_stories_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        updateUI();


        return v;
    }

    private void updateUI() {


        for (int i = 0; i != 20; ++i) {
            Data data = new Data();
            data.setHeader(getResources().getString(R.string.header_text_view) + " " + i);
            data.setBody(getString(R.string.body_text_view));
            arrData.add(data);
        }

        Adapter adapter = new Adapter(arrData);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


    }


    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView mHeaderTextView;
        public TextView mBodyTextView;
        public RatingBar mRatingBar;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            mHeaderTextView = itemView.findViewById(R.id.header_text_view);
            mBodyTextView = itemView.findViewById(R.id.body_text_view);
            mRatingBar = itemView.findViewById(R.id.rating_bar);
        }

        @Override
        public void onClick(View view) {
            int itemPosition = mRecyclerView.getChildLayoutPosition(view);


            HistoryFragment historyFragment = new HistoryFragment();

            Bundle bundle = new Bundle();

            bundle.putSerializable("obj", arrData.get(itemPosition));


            historyFragment.setArguments(bundle);
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, historyFragment)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();

            Toast.makeText(getContext(), "position " + itemPosition, Toast.LENGTH_SHORT).show();

        }
    }

    private class Adapter extends RecyclerView.Adapter<ViewHolder> {
        private ArrayList<Data> mData;

        Adapter(ArrayList<Data> data) {
            mData = data;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_view_item, parent, false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Data data = mData.get(position);

            holder.mHeaderTextView.setText(String.valueOf(data.getHeader()));
            holder.mBodyTextView.setText(String.valueOf(data.getBody()));
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }


    }
}
