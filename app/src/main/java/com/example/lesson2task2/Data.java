package com.example.lesson2task2;

import java.io.Serializable;

public class Data implements Serializable {
    private String mBody;
    private String mHeader;

    public void Data(String header, String body) {
        mHeader = header;
        mBody = body;
    }

    public String getHeader() {
        return mHeader;
    }

    public void setHeader(String mHeader) {
        this.mHeader = mHeader;
    }

    public String getBody() {
        return mBody;
    }

    public void setBody(String mBody) {
        this.mBody = mBody;
    }


}
