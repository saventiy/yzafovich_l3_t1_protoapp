package com.example.lesson2task2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class HistoryFragment extends Fragment {

    private TextView mHistoryHeaderTextView;
    private TextView mHistoryBodyTextView;
    private Button mAddToFavoriteButton;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.history_fragment, container, false);

        Data data = (Data) getArguments().getSerializable("obj");

        mHistoryHeaderTextView = v.findViewById(R.id.history_header_text_view);
        mHistoryHeaderTextView.setText(data.getHeader());

        mHistoryBodyTextView = v.findViewById(R.id.history_body_text_view);
        mHistoryBodyTextView.setText(String.valueOf(data.getBody()));

        mAddToFavoriteButton = v.findViewById(R.id.add_to_favorite_button);
        mAddToFavoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "History was added to favourites", Toast.LENGTH_SHORT).show();
            }
        });

        return v;
    }
}
